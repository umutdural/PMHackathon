import { createApp } from "vue"
import App from "./App.vue"
import VueApexCharts from "vue3-apexcharts";

const app = createApp(App)

app.mount("#app")
// The app.use(VueApexCharts) will make <apexchart> component available everywhere.


app.use(VueApexCharts);

