import { createApp } from "vue"
import VNetworkGraph from "v-network-graph"
import "v-network-graph/lib/style.css"
import App from "./App.vue"
import VueApexCharts from "vue3-apexcharts";
import { VuesticPluginsWithoutComponents } from 'vuestic-ui'
import { VuesticPlugin } from 'vuestic-ui' 
import 'vuestic-ui/dist/vuestic-ui.css' 


const app = createApp(App)

app.use(VNetworkGraph);
app.use(VuesticPlugin) ;
app.use(VuesticPluginsWithoutComponents);
app.mount("#app")


// The app.use(VueApexCharts) will make <apexchart> component available everywhere.


app.use(VueApexCharts);